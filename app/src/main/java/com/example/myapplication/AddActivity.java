package com.example.myapplication;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import java.util.ArrayList;

public class AddActivity extends AppCompatActivity {
    ListView list;
    ArrayList<Contact> contacts;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        updateContent();

        findViewById(R.id.chip_add_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactLoader.addGroup(1);
                updateContent();
            }
        });
        findViewById(R.id.chip_add_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactLoader.addGroup(2);
                updateContent();
            }
        });
        findViewById(R.id.chip_add_3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactLoader.addGroup(3);
                updateContent();
            }
        });
        findViewById(R.id.chip_add_4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactLoader.addGroup(4);
                updateContent();
            }
        });
        findViewById(R.id.chip_add_5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactLoader.addGroup(5);
                updateContent();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void updateContent() {
        contacts = ContactLoader.getUnselectedContacts();
        list = findViewById(R.id.list);
        ArrayAdapter adapter = new ContactListAdapter(this,
                "add", contacts);
        list.setAdapter(adapter);

        ChipGroup group = findViewById(R.id.chipGroup);
        for (int i = 0; i < group.getChildCount(); i++) {
            Chip chip = (Chip) group.getChildAt(i);
            if (ContactLoader.groups[i]) {
                chip.setVisibility(View.GONE);
            }
            else{
                chip.setVisibility(View.VISIBLE);
            }
        }
    }



    // this event will enable the back
    // function to the button on press
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK,returnIntent);
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}