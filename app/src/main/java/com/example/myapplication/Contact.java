package com.example.myapplication;

public class Contact {

    public String name, id;
    public int cat;
    public boolean selected = false;

    public Contact(String id, String name, int cat) {
        this.id = id;
        this.name = name;
        this.cat = cat;
    }
}
