package com.example.myapplication;

import java.util.ArrayList;

public class ContactCategoryMapper {
    /*
       Skelleton class to add actual functionality later on.
       For now categorys are assigned randomly.
     */
    public ContactCategoryMapper() {

    }

    public void mapContacts(ArrayList<Contact> contacts) {
        for (int i = 0; i < contacts.size(); i++) {
            int cat = (int)(Math.random() * 5) + 1;
            contacts.get(i).cat = cat;
        }
    }

}
