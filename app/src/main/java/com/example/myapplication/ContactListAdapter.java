package com.example.myapplication;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.Comparator;


public class ContactListAdapter extends ArrayAdapter<Contact> {
    private final Context context;
    private final ArrayList<Contact> values;
    private String mode;

    private final String[] categorys = {"Cat 1", "Cat 2", "Cat 3", "Cat 4", "Cat 5"};
    private final int[] colors = {Color.rgb(0,255,0),
            Color.rgb(128,255,0),
            Color.rgb(255,255,0),
            Color.rgb(255,128,0),
            Color.rgb(255,0,0)};


    @RequiresApi(api = Build.VERSION_CODES.N)
    public ContactListAdapter(Context context, String mode, ArrayList<Contact> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
        this.mode = mode;
        values.sort(new ContactComparator());
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int inflateViewID;
        if (mode.equals("add")) {
            inflateViewID = R.layout.add_contact;
        }
        else {
            inflateViewID = R.layout.remove_contact;
        }
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(inflateViewID, parent, false);

        TextView nameView = (TextView) rowView.findViewById(R.id.nameView);
        TextView catView = (TextView) rowView.findViewById(R.id.catView);

        String name = values.get(position).name;
        String cat = categorys[values.get(position).cat - 1];
        nameView.setText(name);
        catView.setText(cat);
        catView.setTextColor(colors[values.get(position).cat - 1]);

        Button addButton = (Button) rowView.findViewById(R.id.button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mode.equals("add")) {
                    values.get(position).selected = true;
                    values.remove(position);
                    notifyDataSetChanged();
                }
                else {
                    values.get(position).selected = false;
                    values.remove(position);
                    notifyDataSetChanged();
                }
            }
        });

        return rowView;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void notifyDataSetChanged() {
        //do your sorting here

        values.sort(new ContactComparator());

        super.notifyDataSetChanged();
    }
}

class ContactComparator implements Comparator<Contact> {

    // override the compare() method
    public int compare(Contact s1, Contact s2)
    {
        if (s1.cat == s2.cat) {
            return s1.name.compareTo(s2.name);
        }
        else if (s1.cat > s2.cat)
            return 1;
        else
            return -1;
    }
}
