package com.example.myapplication;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import java.util.ArrayList;

public class ContactLoader {

    private static final int REQUEST_READ_CONTACTS = 79;
    private static ArrayList<Contact> contacts;

    public static int sensitivity;

    public static boolean[] groups = {false, false, false, false, false};


    @SuppressLint("Range")
    public static void loadAllContacts(Context context) {
        ArrayList<Contact> nameList = new ArrayList<Contact>();
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));
                Contact contact = new Contact(id, name, -1);
                nameList.add(contact);
            }
        }
        if (cur != null) {
            cur.close();
        }
        ContactCategoryMapper mapper = new ContactCategoryMapper();
        mapper.mapContacts(nameList);
        contacts = nameList;
    }

    public static void setNewSelection(int sens) {
        sensitivity = sens;
        int level = 6 - sens;
        for (int i = 0; i < groups.length; i++) {
            groups[i] = i < level;
        }
        for (int i = 0; i < contacts.size(); i++) {
            Contact tmp = contacts.get(i);
            if (tmp.cat <= level) {
                tmp.selected = true;
            }
            else {
                tmp.selected = false;
            }
        }
    }

    public static void addGroup(int num) {
        groups[num-1] = true;
        for (int i = 0; i < contacts.size(); i++) {
            Contact tmp = contacts.get(i);
            if (tmp.cat == num) {
                tmp.selected = true;
            }
        }
    }

    public static void removeGroup(int num) {
        groups[num-1] = false;
        for (int i = 0; i < contacts.size(); i++) {
            Contact tmp = contacts.get(i);
            if (tmp.cat == num) {
                tmp.selected = false;
            }
        }
    }

    public static ArrayList<Contact> getAllContacts() {
        return contacts;
    }

    public static ArrayList<Contact> getSelectedContacts() {
        ArrayList<Contact> selectedContacts = new ArrayList<Contact>();
        for (int i = 0; i < contacts.size(); i++) {
            if (contacts.get(i).selected) {
                selectedContacts.add(contacts.get(i));
            }
        }
        return selectedContacts;
    }

    public static ArrayList<Contact> getUnselectedContacts() {
        ArrayList<Contact> unselectedContacts = new ArrayList<Contact>();
        for (int i = 0; i < contacts.size(); i++) {
            if (!contacts.get(i).selected) {
                unselectedContacts.add(contacts.get(i));
            }
        }
        return unselectedContacts;
    }
}
