package com.example.myapplication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.view.View;

import java.util.ArrayList;

public class PaintView extends View {

    Paint circlePaint, highlightPaint, dotSelectedPaint, dotUnselectedPaint, textPaint, backgroudPaint;

    private float[] r = new float[5];
    private int[] colors;

    private int sensitivity;

    @SuppressLint("ResourceAsColor")
    public PaintView(Context context) {
        super(context);

        DisplayMetrics displayMetrics = new DisplayMetrics();

        ((Activity) getContext()).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);

        circlePaint = new Paint();
        circlePaint.setStyle(Paint.Style.STROKE);
        circlePaint.setStrokeWidth(5);
        highlightPaint = new Paint();
        highlightPaint.setStyle(Paint.Style.FILL);
        dotSelectedPaint = new Paint();
        dotSelectedPaint.setStyle(Paint.Style.FILL);
        dotUnselectedPaint = new Paint();
        dotUnselectedPaint.setStyle(Paint.Style.FILL);
        textPaint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        textPaint.setTextSize(pxFromDp(context, 24));
        backgroudPaint = new Paint();
        backgroudPaint.setStyle(Paint.Style.FILL);

        int currentNightMode = getContext().getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
        switch (currentNightMode) {
            case Configuration.UI_MODE_NIGHT_NO:
                // Night mode is not active, we're using the light theme
                circlePaint.setColor(Color.BLACK);
                highlightPaint.setColor(Color.LTGRAY);
                dotSelectedPaint.setColor(Color.BLACK);
                dotUnselectedPaint.setColor(Color.GRAY);
                textPaint.setColor(Color.BLACK);
                backgroudPaint.setColor(Color.WHITE);
                break;
            case Configuration.UI_MODE_NIGHT_YES:
                circlePaint.setColor(Color.WHITE);
                highlightPaint.setColor(Color.DKGRAY);
                dotSelectedPaint.setColor(Color.WHITE);
                dotUnselectedPaint.setColor(Color.GRAY);
                textPaint.setColor(Color.WHITE);
                backgroudPaint.setColor(Color.BLACK);
                break;
        }

        colors = new int[5];
        colors[0] = Color.rgb(0,255,0);
        colors[1] = Color.rgb(128,255,0);
        colors[2] = Color.rgb(255,255,0);
        colors[3] = Color.rgb(255,128,0);
        colors[4] = Color.rgb(255,0,0);

    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(0,20, getWidth(), 120, highlightPaint);
        float rwidth = getWidth()/6;
        float delta = rwidth/6;
        Paint rectPaint = new Paint();
        for (int i = 0; i < 5; i++) {
            if (sensitivity >= i+1) {
                rectPaint.setColor(colors[i]);
            }
            else {
                rectPaint.setColor(backgroudPaint.getColor());
            }
            canvas.drawRect(delta + (rwidth+delta)*i, 35, delta + rwidth + (rwidth+delta)*i, 105, rectPaint);
        }

        // draw Circles
        r = getCircles(getWidth());
        if (sensitivity > 0 && sensitivity < 6){
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, r[sensitivity - 1], highlightPaint);
        }
        for (int i = 0; i < 5; i++) {
            canvas.drawCircle(getWidth()/2, getHeight()/2, r[i], circlePaint);
        }
        canvas.drawCircle(getWidth()/2, getHeight()/2, (float) (getWidth()*0.1/2), dotSelectedPaint);
        //String str = Integer.toString(sensitivity);
        //canvas.drawText(str, getWidth()/2, getHeight()-20, textPaint);

        ArrayList<Contact> selected = ContactLoader.getSelectedContacts();
        ArrayList<Contact> unselected = ContactLoader.getUnselectedContacts();

        int cx = getWidth()/2;
        int cy = getHeight()/2;

        for (int i = 0; i < unselected.size(); i++) {
            Contact contact = unselected.get(i);
            double d = r[5-contact.cat] + (r[2] - r[1]) * (0.25 + Math.random()/2);
            int angle = (int)(Math.random() * 360);
            int x = (int)(cx + d * Math.cos(-angle*Math.PI/180));
            int y = (int)(cy + d * Math.sin(-angle*Math.PI/180));
            canvas.drawCircle(x, y, 5, dotUnselectedPaint);
        }

        for (int i = 0; i < selected.size(); i++) {
            Contact contact = selected.get(i);
            double d = r[5-contact.cat] + (r[2] - r[1]) * (0.25 + Math.random()/2);
            int angle = (int)(Math.random() * 360);
            int x = (int)(cx + d * Math.cos(-angle*Math.PI/180));
            int y = (int)(cy + d * Math.sin(-angle*Math.PI/180));
            canvas.drawCircle(x, y, 7, dotSelectedPaint);
        }
    }


    public void setSensitivity(int s) {
        if (s > 0 && s < 6) {
            sensitivity = s;
        }
    }

    private float[] getCircles(double width) {
        float[] r = new float[5];
        for (int i = 0; i < 5; i++) {
            float radius = (float) (width/2*(0.9 - i*0.15));
            r[i] = radius;
        }
        return r;
    }
}