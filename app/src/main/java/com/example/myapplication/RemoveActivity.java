package com.example.myapplication;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class RemoveActivity extends AppCompatActivity {
    ListView list;
    ArrayList<Contact> contacts;
    FloatingActionButton fab;

    private int[] colors = {Color.rgb(0,255,0),
            Color.rgb(128,255,0),
            Color.rgb(255,255,0),
            Color.rgb(255,128,0),
            Color.rgb(255,0,0)};


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remove);

        updateContent();

        fab = findViewById(R.id.floatingAdd);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RemoveActivity.this, AddActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        findViewById(R.id.chip_remove_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactLoader.removeGroup(1);
                updateContent();
            }
        });
        findViewById(R.id.chip_remove_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactLoader.removeGroup(2);
                updateContent();
            }
        });
        findViewById(R.id.chip_remove_3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactLoader.removeGroup(3);
                updateContent();
            }
        });
        findViewById(R.id.chip_remove_4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactLoader.removeGroup(4);
                updateContent();
            }
        });
        findViewById(R.id.chip_remove_5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactLoader.removeGroup(5);
                updateContent();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void updateContent() {

        contacts = ContactLoader.getSelectedContacts();
        list = findViewById(R.id.list);
        ArrayAdapter adapter = new ContactListAdapter(this,
                "remove", contacts);
        list.setAdapter(adapter);

        ChipGroup group = findViewById(R.id.chipGroup);
        for (int i = 0; i < group.getChildCount(); i++) {
            Chip chip = (Chip) group.getChildAt(i);
            if (ContactLoader.groups[i]) {
                chip.setVisibility(View.VISIBLE);
            }
            else{
                chip.setVisibility(View.GONE);
            }
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        updateContent();

    } //onActivityResult


    // this event will enable the back
    // function to the button on press
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
